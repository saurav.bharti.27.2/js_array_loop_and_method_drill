
const got = require('./data')

function countAllPeople(){

    let totalPeople = 0;
    
    for( const houses in got){
    
        for( const eachHouse in got[houses]){
    
            totalPeople += got[houses][eachHouse].people.length
    
        }
    
    }

    return totalPeople
}

function peopleByHouses(){

    const peopleByEachHouse = {}
    
    for (const houses in got){
    
        for (const eachHouse in got[houses]){
    
            peopleByEachHouse[got[houses][eachHouse].name] = got[houses][eachHouse].people.length
        }
    }

    return peopleByEachHouse;

}

function everyone(){
    
    const nameOfAllPeople = []
    
    for (const houses in got){
    
        for(const eachHouse in got[houses]){
    
            for(const individualPeople of got[houses][eachHouse].people){
    
                nameOfAllPeople.push(individualPeople.name)
    
            }
    
        }
    }

    return nameOfAllPeople
}

function nameWithS(){

    const peopleWithSInNames = []

    for (const houses in got){
    
        for(const eachHouse in got[houses]){
    
            for(const individualPeople of got[houses][eachHouse].people){
                
                if((individualPeople.name.toLowerCase()).includes('s')){

                    peopleWithSInNames.push(individualPeople.name)

                }
    
            }
    
        }
    }

    return peopleWithSInNames;

}

function nameWithA(){

    const peopleWithAInNames = []

    for (const houses in got){
    
        for(const eachHouse in got[houses]){
    
            for(const individualPeople of got[houses][eachHouse].people){
                
                if((individualPeople.name.toLowerCase()).includes('a')){

                    peopleWithAInNames.push(individualPeople.name)

                }
    
            }
    
        }
    }

    return peopleWithAInNames;

}

function surnameWithS(){

    const peopleWithSInSurname = []

    for (const houses in got){
    
        for(const eachHouse in got[houses]){
    
            for(const individualPeople of got[houses][eachHouse].people){
                
                let nameOfIndividual = individualPeople.name

                let firstNameAndSurname = nameOfIndividual.split(' ')

                let last = firstNameAndSurname.length -1

                let surname = firstNameAndSurname[last]

                if(surname[0] === 'S'){

                    peopleWithSInSurname.push(individualPeople.name)

                }
    
            }
    
        }
    }

    return peopleWithSInSurname
}

function surnameWithA(){

    const peopleWithAInSurname = []

    for (const houses in got){
    
        for(const eachHouse in got[houses]){
    
            for(const individualPeople of got[houses][eachHouse].people){
                
                let nameOfIndividual = individualPeople.name

                let firstNameAndSurname = nameOfIndividual.split(' ')

                let surname = firstNameAndSurname[1]

                if(surname[0] === 'A'){

                    peopleWithAInSurname.push(individualPeople.name)

                }
    
            }
    
        }
    }

    return peopleWithAInSurname
}

function peopleNameOfAllHouses(){

    const objectWithHouseAndNames = {}

    for(const houses in got){

        for(const eachHouse in got[houses]){

            if( !objectWithHouseAndNames.hasOwnProperty( got[houses][eachHouse].name ) ){

                objectWithHouseAndNames[got[houses][eachHouse].name] = [] 
            }

            for(const individualPeople of got[houses][eachHouse].people){

                objectWithHouseAndNames[got[houses][eachHouse].name].push(individualPeople.name)

            }

        }

    }
    
    return objectWithHouseAndNames

}